//
//  INOUserAnnotation.m
//  InoMaps
//
//  Created by Vladislav Grigoriev on 12/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "INOUserAnnotation.h"

@implementation INOUserAnnotation

@synthesize coordinate = _coordinate;
@synthesize title = _title;
@synthesize subtitle = _subtitle;

- (instancetype)init {
    self = [super init];
    if (self) {
        _title = @"User Object";
        _subtitle = @"Subtitle";
    }
    return self;
}

- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate {
    _coordinate = newCoordinate;
}

@end
